package com.tsquare.maven.plugins.appconfig.export.properties;

import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

public class SortedProperties extends Properties {

   private final transient Comparator<Object> comparator;

   public SortedProperties(Comparator comparator) {
      this.comparator = comparator;
   }

   public SortedProperties(Properties defaults, Comparator comparator) {
      super(defaults);
      this.comparator = comparator;
   }

   public SortedProperties() {
      this(Comparator.naturalOrder());
   }

   public SortedProperties(Properties defaults) {
      this(defaults, Comparator.naturalOrder());
   }

   @Override
   public Set<Entry<Object, Object>> entrySet() {
      TreeSet<Entry<Object, Object>> treeSet =
            new TreeSet<Entry<Object, Object>>((o1, o2) -> comparator.compare(o1.getKey(), o2.getKey()));
      treeSet.addAll(super.entrySet());

      return treeSet;
   }

   @Override
   public synchronized Enumeration<Object> keys() {
      TreeSet<Object> treeSet = new TreeSet<Object>(comparator);
      treeSet.addAll(super.keySet());

      return Collections.enumeration(treeSet);
   }
}
