package com.tsquare.maven.plugins.appconfig.documentation;

import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsquare.maven.plugins.appconfig.AbstractMojo;
import com.tsquare.maven.plugins.appconfig.documentation.exporters.DocumentationFactory;
import com.tsquare.maven.plugins.appconfig.documentation.exporters.DocumentationFormat;
import com.tsquare.maven.plugins.appconfig.documentation.model.ConfigurationMetadata;
import com.tsquare.maven.plugins.appconfig.documentation.model.Group;

/**
 * Generates site documentation for Spring-boot enabled @ConfigurationProperties configuration points.
 * <p>
 * The mojo should be enabled in modules which have {@value #CONFIGURATION_METADATA_FILENAME} files generated
 * by org.springframework.boot:spring-boot-configuration-processor or dependencies on compile classpath with
 * such metadata files.
 * <p>
 * The mojo is executed during the pre-site phase.
 */
@Mojo(name = "documentation", defaultPhase = LifecyclePhase.PRE_SITE,
      requiresDependencyResolution = ResolutionScope.COMPILE)
public class DocumentationMojo extends AbstractMojo {

   private static final String CONFIGURATION_METADATA_FILENAME = "spring-configuration-metadata.json";

   /** Skip mojo if true. */
   @Parameter(property = "appconfig.skipDocumentation", required = true, defaultValue = "false")
   private boolean skip;

   /** Generate documentation in the defined directory. */
   @Parameter(property = "appconfig.documentationPath", required = true,
              defaultValue = "${project.build.directory}/generated-site/asciidoc/appconfig")
   private String documentationPath;

   /**
    * Whether generated documentation is for external viewers. If {@code true}, internal details are hidden
    * (Java types, ...).
    */
   @Parameter(property = "appconfig.external", required = true, defaultValue = "false")
   private boolean external;

   /** Documentation format. */
   @Parameter(property = "appconfig.documentationFormat", required = true, defaultValue = "ASCIIDOC")
   private DocumentationFormat documentationFormat;

   /** Whether include a configuration point TOC or not. */
   @Parameter(property = "appconfig.toc", required = true, defaultValue = "true")
   private boolean toc;

   /** Includes configuration group names matching theses regexps. Includes are taken in priority on excludes. */
   @Parameter(property = "appconfig.includes", required = false)
   private List<String> includes = new ArrayList<>();

   /** Excludes configuration group names matching theses regexps. Includes are taken in priority on excludes. */
   @Parameter(property = "appconfig.excludes", required = false)
   private List<String> excludes = new ArrayList<>();

   @Override
   public void execute() throws MojoExecutionException {
      if (!skip) {
         try {
            DocumentationFactory
                  .instance(documentationFormat,
                            getLog(),
                            configurationMetadata(),
                            external,
                            toc,
                            Paths.get(documentationPath))
                  .documentation();
         } catch (Exception e) {
            throw new MojoExecutionException(e.getMessage(), e);
         }
      }
   }

   private ConfigurationMetadata configurationMetadata() throws IOException {
      ConfigurationMetadata metadata = new ConfigurationMetadata();
      for (ConfigurationMetadata extractedConfigurationMetadata : extractConfigurationMetadatas()) {
         metadata = metadata.merge(filter(extractedConfigurationMetadata));
      }
      return metadata;
   }

   private ConfigurationMetadata filter(ConfigurationMetadata configurationMetadata) {
      List<Pattern> includePatterns = includes.stream().map(Pattern::compile).collect(toList());
      List<Pattern> excludePatterns = excludes.stream().map(Pattern::compile).collect(toList());

      if (configurationMetadata.getGroups() != null) {
         return configurationMetadata.setGroups(configurationMetadata
                                                      .getGroups()
                                                      .stream()
                                                      .filter(filterGroup(includePatterns, excludePatterns))
                                                      .collect(toList()));
      } else {
         return configurationMetadata;
      }
   }

   private Predicate<Group> filterGroup(List<Pattern> includes, List<Pattern> excludes) {
      return group -> excludes.stream().noneMatch(exclude -> exclude.matcher(group.getName()).matches())
                      || includes.stream().anyMatch(include -> include.matcher(group.getName()).matches());
   }

   private List<ConfigurationMetadata> extractConfigurationMetadatas() throws IOException {
      loadModuleClasspath();

      List<ConfigurationMetadata> configurationMetadataMetadatas = new ArrayList<>();
      Enumeration<URL> resources = classLoader().getResources("META-INF/" + CONFIGURATION_METADATA_FILENAME);

      while (resources.hasMoreElements()) {
         configurationMetadataMetadatas.add(parseConfigurationMetadata(extractMetadata(resources.nextElement())));
      }

      return configurationMetadataMetadatas;
   }

   private String extractMetadata(URL configurationMetadata) {
      try (InputStream inputStream = configurationMetadata.openStream()) {
         return IOUtils.toString(inputStream);
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }

   }

   private ConfigurationMetadata parseConfigurationMetadata(String configurationMetadata) throws IOException {
      ObjectMapper mapper = new ObjectMapper();

      return mapper.readValue(configurationMetadata, ConfigurationMetadata.class);
   }

}
