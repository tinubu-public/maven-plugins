package com.tsquare.maven.plugins.appconfig.export;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.net.UnknownHostException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Stream;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.CompositePropertySource;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.DefaultPropertySourceFactory;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.core.io.support.PropertySourceFactory;
import org.springframework.core.io.support.ResourcePropertySource;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.util.StringUtils;

import com.tsquare.maven.plugins.appconfig.AbstractMojo;
import com.tsquare.maven.plugins.appconfig.export.exporters.ConfigExporter;
import com.tsquare.maven.plugins.appconfig.export.exporters.ConfigExporterFactory;
import com.tsquare.maven.plugins.appconfig.export.exporters.ExportFormat;
import com.tsquare.maven.plugins.appconfig.export.filters.IncludePatternKeyFilter;
import com.tsquare.maven.plugins.appconfig.export.filters.IncludePrefixKeyFilter;
import com.tsquare.maven.plugins.appconfig.export.filters.KeyFilter;

/**
 * Export actual configuration values for a given instance to defined format.
 */
@Mojo(name = "export", defaultPhase = LifecyclePhase.COMPILE,
      requiresDependencyResolution = ResolutionScope.COMPILE)
public class ExportMojo extends AbstractMojo {

   private static final PropertySourceFactory DEFAULT_PROPERTY_SOURCE_FACTORY =
         new DefaultPropertySourceFactory();
   private static final String CONFIGURATION_RESOURCES_PATH = "config";
   private static final String OBFUSCATE_MASK = "********";

   /** Skip mojo if true. */
   @Parameter(property = "appconfig.skipExport", required = true, defaultValue = "false")
   private boolean skip;

   /** Export directory path. */
   @Parameter(property = "appconfig.exportPath", required = true,
              defaultValue = "${project.build.directory}/appconfig")
   private String exportPath;

   /** Export format. */
   @Parameter(property = "appconfig.exportFormat", required = true, defaultValue = "PROPERTIES")
   private ExportFormat exportFormat;

   /**
    * Whether existing values from other modules should be merged. If values are merged, you should configure
    * {@code appconfig.exportPath} to the same value for all modules (e.g.: {@code
    * ${maven.multiModuleProjectDirectory}/target/appconfig}).
    */
   @Parameter(property = "appconfig.merge", required = true, defaultValue = "false")
   private boolean merge;

   /**
    * Export configuration for defined instances. Exported files are suffixed with {@code
    * instance-environment}. Do nothing if empty.
    */
   @Parameter(property = "appconfig.instances")
   private List<String> instances;

   /**
    * Export configuration for defined environments. Exported files are suffixed with {@code
    * instance-environment}. Do nothing if empty.
    */
   @Parameter(property = "appconfig.environments")
   private List<String> environments;

   /** Only include keys matching defined prefixes or patterns. */
   @Parameter(property = "appconfig.includePrefixes")
   private List<String> includePrefixes;

   /** Exclude keys matching defined prefixes. */
   @Parameter(property = "appconfig.excludePrefixes")
   private List<String> excludePrefixes;

   /** Only include keys matching defined prefixes or patterns. */
   @Parameter(property = "appconfig.includePatterns")
   private List<String> includePatterns;

   /** Exclude keys matching defined patterns. */
   @Parameter(property = "appconfig.excludePatterns")
   private List<String> excludePatterns;

   /** Obfuscate keys matching defined prefixes or patterns. */
   @Parameter(property = "appconfig.obfuscatePatterns")
   private List<String> obfuscatePatterns;

   /** Obfuscate keys matching defined prefixes or patterns. */
   @Parameter(property = "appconfig.obfuscatePrefixes")
   private List<String> obfuscatePrefixes;

   @Override
   public void execute() throws MojoExecutionException {
      if (!skip) {
         loadModuleClasspath();

         try {
            for (String instance : instances) {
               for (String environment : environments) {
                  ConfigExporter exporter =
                        ConfigExporterFactory.instance(exportFormat, getLog(), Paths.get(exportPath));

                  exporter.export(instance,
                                  environment,
                                  obfuscate(generateReferential(instance,
                                                                environment,
                                                                properties(propertySources(instance,
                                                                                           environment)))));
               }
            }
         } catch (Exception e) {
            throw new MojoExecutionException(e.getMessage(), e);
         }
      }
   }

   private Properties obfuscate(Properties properties) {
      Properties obfuscatedProperties = new Properties();

      KeyFilter obfuscateFilter = obfuscateKeyFilter();

      properties.forEach((key, value) -> obfuscatedProperties.put(key,
                                                                  obfuscateFilter.test((String) key)
                                                                  ? OBFUSCATE_MASK
                                                                  : value));

      return obfuscatedProperties;
   }

   @SuppressWarnings("ResultOfMethodCallIgnored")
   private Properties generateReferential(String instance, String environment, Properties properties)
         throws IOException {

      Properties referential;

      if (merge) {
         String identifier = String.format("%s-%s-%s", instance, environment, uniqueExecutionId());
         File referentialFile = mergeReferentialFile(identifier);

         getLog().debug(String.format("Merge referential instance '%s' environment '%s' to '%s'",
                                      instance,
                                      environment,
                                      referentialFile));

         referential = new Properties();

         if (merge) {
            try {
               referential.loadFromXML(new FileInputStream(referentialFile));
            } catch (FileNotFoundException __) {
               /* pass */
            }
         }

         properties.forEach(referential::put);

         referentialFile.getParentFile().mkdirs();
         referential.storeToXML(new FileOutputStream(referentialFile), identifier);
      } else {
         referential = properties;
      }

      return referential;
   }

   private File mergeReferentialFile(String identifier) {
      return Paths
            .get(exportPath)
            .resolve("referential")
            .resolve(String.format("referential-%s.xml", identifier))
            .toFile();
   }

   private String uniqueExecutionId() {
      return ManagementFactory.getRuntimeMXBean().getName();
   }

   private Properties properties(CompositePropertySource propertySources) {
      Properties properties = new Properties();

      Stream.of(propertySources.getPropertyNames()).filter(keyFilter()).forEach(key -> {
         String newValue = propertySources.getProperty(key).toString();

         if (merge) {
            String existingValue = properties.getProperty(key);

            if (existingValue != null && !existingValue.equals(newValue)) {
               throw new IllegalStateException(String.format(
                     "Already referenced property '%s' value is unexpectedly different : '%s' -> '%s'",
                     key,
                     existingValue,
                     newValue));
            }
         }

         properties.put(key, newValue);
      });

      return properties;
   }

   private KeyFilter keyFilter() {
      KeyFilter filter = __ -> includePatterns.isEmpty() && includePrefixes.isEmpty();

      filter = includePatterns
            .stream()
            .map(IncludePatternKeyFilter::new)
            .map(KeyFilter.class::cast)
            .reduce(filter, KeyFilter::or);
      filter = includePrefixes
            .stream()
            .map(IncludePrefixKeyFilter::new)
            .map(KeyFilter.class::cast)
            .reduce(filter, KeyFilter::or);
      filter = excludePatterns
            .stream()
            .map(IncludePatternKeyFilter::new)
            .map(IncludePatternKeyFilter::negate)
            .map(KeyFilter.class::cast)
            .reduce(filter, KeyFilter::and);
      filter = excludePrefixes
            .stream()
            .map(IncludePrefixKeyFilter::new)
            .map(IncludePrefixKeyFilter::negate)
            .map(KeyFilter.class::cast)
            .reduce(filter, KeyFilter::and);

      return filter;
   }

   private KeyFilter obfuscateKeyFilter() {
      KeyFilter filter = __ -> false;

      filter = obfuscatePatterns
            .stream()
            .map(IncludePatternKeyFilter::new)
            .map(KeyFilter.class::cast)
            .reduce(filter, KeyFilter::or);
      filter = obfuscatePrefixes
            .stream()
            .map(IncludePrefixKeyFilter::new)
            .map(KeyFilter.class::cast)
            .reduce(filter, KeyFilter::or);

      return filter;
   }

   @SafeVarargs
   private final <T> Stream<T> of(Stream<T> stream, Stream<T>... streams) {
      return Arrays.stream(streams).reduce(stream, Stream::concat);
   }

   private CompositePropertySource propertySources(String instance, String environment) {

      getLog().debug(String.format("Retrieve property sources for instance '%s' environment '%s'",
                                   instance,
                                   environment));

      CompositePropertySource propertySources =
            new CompositePropertySource(String.format("environment-%s-%s", instance, environment));
      String instanceConfiguration = String.format("%s/%s", instance, "application");
      String environmentConfiguration = String.format("%s/%s-%s", instance, "application", environment);

      loadConfigurationSource(propertySources, environmentConfiguration, true);
      loadConfigurationSource(propertySources, instanceConfiguration, true);
      loadConfigurationSource(propertySources, "default", true);
      loadAnnotatedPropertySources(propertySources);

      return propertySources;
   }

   private void loadConfigurationSource(CompositePropertySource propertySources,
                                        String name,
                                        boolean ignoreResourceNotFound) {
      propertiesConfigurationSource(name,
                                    ignoreResourceNotFound).ifPresent(propertySources::addPropertySource);
      yamlConfigurationSource(name,
                              ignoreResourceNotFound).ifPresent(propertySources::addPropertySource);
   }

   private void loadAnnotatedPropertySources(CompositePropertySource propertySources) {
      ClassPathScanningCandidateComponentProvider scanner =
            new ClassPathScanningCandidateComponentProvider(false);

      scanner.addIncludeFilter(new AnnotationTypeFilter(PropertySource.class));

      Set<BeanDefinition> propertySourcesComponents = scanner.findCandidateComponents("com.tsquare");

      getLog().debug("Found components defining @PropertySource : '" + propertySourcesComponents
            .stream()
            .map(BeanDefinition::getBeanClassName)
            .collect(joining(",")) + "'");

      List<? extends Class<?>> propertySourcesClasses = propertySourcesComponents.stream().map(bd -> {
         try {
            return classLoader().loadClass(bd.getBeanClassName());
         } catch (ClassNotFoundException e) {
            throw new IllegalStateException(e);
         }
      }).collect(toList());

      try {
         for (Class<?> propertySourceClass : propertySourcesClasses) {
            propertySourceConfigurationSource(propertySourceClass.getAnnotation(PropertySource.class)).forEach(
                  propertySources::addPropertySource);
         }
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }

      getLog().debug("Found annotated property sources : '" + propertySources
            .getPropertySources()
            .stream()
            .map(org.springframework.core.env.PropertySource::getName)
            .collect(joining()) + "'");
   }

   private List<org.springframework.core.env.PropertySource<?>> propertySourceConfigurationSource(
         PropertySource propertySource) throws IOException {
      String name = propertySource.name();
      if (!StringUtils.hasLength(name)) {
         name = null;
      }
      String encoding = propertySource.encoding();
      if (!StringUtils.hasLength(encoding)) {
         encoding = null;
      }
      String[] locations = propertySource.value();

      boolean ignoreResourceNotFound = propertySource.ignoreResourceNotFound();

      Class<? extends PropertySourceFactory> factoryClass = propertySource.factory();
      PropertySourceFactory factory = (factoryClass == PropertySourceFactory.class
                                       ? DEFAULT_PROPERTY_SOURCE_FACTORY
                                       : BeanUtils.instantiateClass(factoryClass));

      List<org.springframework.core.env.PropertySource<?>> propertySources = new ArrayList<>();
      for (String location : locations) {
         try {
            Resource resource = resourceLoader().getResource(location);
            propertySources.add(factory.createPropertySource(name, new EncodedResource(resource, encoding)));
         } catch (IllegalArgumentException | FileNotFoundException | UnknownHostException ex) {
            if (ignoreResourceNotFound) {
               if (getLog().isInfoEnabled()) {
                  getLog().debug("Properties location [" + location + "] not resolvable: " + ex.getMessage());
               }
            } else {
               throw ex;
            }
         }
      }

      return propertySources;
   }

   private Optional<org.springframework.core.env.PropertySource<?>> propertiesConfigurationSource(String name,
                                                                                                  boolean ignoreResourceNotFound) {
      String resource = CONFIGURATION_RESOURCES_PATH + "/" + name + ".properties";

      try {
         ResourcePropertySource source = new ResourcePropertySource("classpath:" + resource, classLoader());

         getLog().info(String.format("Load configuration source '%s'", resource));

         return Optional.of(source);
      } catch (IOException e) {
         if (!ignoreResourceNotFound) {
            throw new IllegalStateException(e);
         }
      }

      getLog().debug(String.format("Optional configuration '%s' not found", resource));

      return Optional.empty();
   }

   private Optional<org.springframework.core.env.PropertySource<?>> yamlConfigurationSource(String name,
                                                                                            boolean ignoreResourceNotFound) {
      String resource = CONFIGURATION_RESOURCES_PATH + "/" + name + ".yml";

      ClassPathResource yamlResource = new ClassPathResource(resource, classLoader());

      if (yamlResource.exists() || !ignoreResourceNotFound) {

         YamlPropertiesFactoryBean yamlLoader = new YamlPropertiesFactoryBean();
         yamlLoader.setResources(yamlResource);

         PropertiesPropertySource source = new PropertiesPropertySource(name, yamlLoader.getObject());

         getLog().info(String.format("Load configuration source '%s'", resource));

         return Optional.of(source);
      } else {
         getLog().debug(String.format("Optional configuration '%s' not found", resource));

         return Optional.empty();
      }
   }

}
