package com.tsquare.maven.plugins.appconfig.export.exporters;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

public interface ConfigExporter {
   void export(String instance, String environment, Properties properties) throws IOException;

   default String currentDateTime() {
      return DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(ZonedDateTime.now());
   }
}
