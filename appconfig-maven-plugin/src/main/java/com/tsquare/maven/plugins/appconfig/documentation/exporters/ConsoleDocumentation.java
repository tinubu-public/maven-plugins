package com.tsquare.maven.plugins.appconfig.documentation.exporters;

import org.apache.maven.plugin.logging.Log;

import com.tsquare.maven.plugins.appconfig.documentation.model.ConfigurationMetadata;

public class ConsoleDocumentation implements Documentation {

   private final Log log;
   private final ConfigurationMetadata metadata;
   private final boolean external;

   public ConsoleDocumentation(Log log, ConfigurationMetadata metadata, boolean external) {
      this.log = log;
      this.metadata = metadata;
      this.external = external;
   }

   @Override
   public void documentation() {
      log.info("-- Display configuration documentation");
      log.info("");

      metadata.getGroups().forEach(group -> {
         log.info(String.format("== [%s] %s",
                                group.getName(),
                                group.getDescription() != null ? group.getDescription() : ""));
         metadata
               .getProperties()
               .stream()
               .filter(property -> property.getSourceType().equals(group.getType()))
               .forEach(property -> log.info(String.format("%s %s%s : %s",
                                                           property.getName(),
                                                           external ? "" : "(" + property.getType() + ")",
                                                           property.getDefaultValue() != null ? " ["
                                                                                                + property.getDefaultValue()
                                                                                                + "]" : "",
                                                           property.getDescription())));
         log.info("");
      });
   }
}
