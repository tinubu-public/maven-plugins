package com.tsquare.maven.plugins.appconfig.documentation.exporters;

import java.nio.file.Path;

import org.apache.maven.plugin.logging.Log;

import com.tsquare.maven.plugins.appconfig.documentation.model.ConfigurationMetadata;

public final class DocumentationFactory {

   private DocumentationFactory() {}

   public static Documentation instance(DocumentationFormat documentationFormat,
                                        Log log,
                                        ConfigurationMetadata metadata,
                                        boolean external,
                                        boolean toc,
                                        Path documentationPath) {
      switch (documentationFormat) {
         case CONSOLE:
            return new ConsoleDocumentation(log, metadata, external);
         case ASCIIDOC:
            return new AsciidocDocumentation(log, metadata, external, toc, documentationPath);
         case MARKDOWN:
            return new MarkdownDocumentation(log, metadata, external, toc, documentationPath);
         default:
            throw new IllegalStateException(String.format("Unknown documentation format '%s'",
                                                          documentationPath));
      }
   }
}
