package com.tsquare.maven.plugins.appconfig.documentation.exporters;

public enum DocumentationFormat {
   CONSOLE, ASCIIDOC, MARKDOWN
}
