package com.tsquare.maven.plugins.appconfig.documentation.model;

import java.util.Objects;
import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Group {
   String sourceType;
   String type;
   String name;
   String description;

   public String getSourceType() {
      return sourceType;
   }

   public Group setSourceType(String sourceType) {
      this.sourceType = sourceType;
      return this;
   }

   public String getType() {
      return type;
   }

   public Group setType(String type) {
      this.type = type;
      return this;
   }

   public String getName() {
      return name;
   }

   public Group setName(String name) {
      this.name = name;
      return this;
   }

   public String getDescription() {
      return description;
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", Group.class.getSimpleName() + "[", "]")
            .add("sourceType='" + sourceType + "'")
            .add("type='" + type + "'")
            .add("name='" + name + "'")
            .add("description='" + description + "'")
            .toString();
   }

   public Group setDescription(String description) {
      this.description = description;
      return this;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Group group = (Group) o;
      return Objects.equals(name, group.name);
   }

   @Override
   public int hashCode() {
      return Objects.hash(name);
   }
}
