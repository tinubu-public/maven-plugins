package com.tsquare.maven.plugins.appconfig.export.filters;

import java.util.regex.Pattern;

public class IncludePatternKeyFilter implements KeyFilter {
   private final Pattern includePattern;

   public IncludePatternKeyFilter(String includePattern) {
      this.includePattern = Pattern.compile(includePattern);
   }

   @Override
   public boolean test(String key) {
      return includePattern.matcher(key).matches();
   }

}
