package com.tsquare.maven.plugins.appconfig.export.exporters;

import java.nio.file.Path;

import org.apache.maven.plugin.logging.Log;

public final class ConfigExporterFactory {

   private ConfigExporterFactory() {}

   public static ConfigExporter instance(ExportFormat exportFormat, Log log, Path exportPath) {
      switch (exportFormat) {
         case CONSOLE:
            return new ConsoleConfigExporter(log);
         case PROPERTIES:
            return new PropertiesConfigExporter(log, exportPath);
         case CSV:
            return new CsvConfigExporter(log, exportPath);
         case XML:
            return new XmlConfigExporter(log, exportPath);
         default:
            throw new IllegalStateException(String.format("Unknown export format '%s'", exportFormat));
      }
   }
}
