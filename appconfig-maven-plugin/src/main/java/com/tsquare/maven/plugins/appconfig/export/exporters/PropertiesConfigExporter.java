package com.tsquare.maven.plugins.appconfig.export.exporters;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Properties;

import org.apache.maven.plugin.logging.Log;

import com.tsquare.maven.plugins.appconfig.export.properties.SortedProperties;

public class PropertiesConfigExporter implements ConfigExporter {

   private final Log log;
   private final Path exportPath;

   public PropertiesConfigExporter(Log log, Path exportPath) {
      this.log = log;
      this.exportPath = exportPath;
   }

   @Override
   @SuppressWarnings("ResultOfMethodCallIgnored")
   public void export(String instance, String environment, Properties properties) throws IOException {
      String identifier = String.format("%s-%s", instance, environment);
      File exportFile = exportPath.resolve(String.format("appconfig-%s.properties", identifier)).toFile();

      log.info(String.format("-- Export instance '%s' environment '%s' to '%s'",
                             instance,
                             environment,
                             exportFile));

      SortedProperties sortedProperties = new SortedProperties();
      properties.forEach(sortedProperties::put);

      exportFile.getParentFile().mkdirs();
      sortedProperties.store(new FileOutputStream(exportFile), identifier);
   }

}
