package com.tsquare.maven.plugins.appconfig.documentation.exporters;

import static java.util.Collections.nCopies;
import static java.util.stream.Collectors.joining;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.TreeMap;
import java.util.regex.Pattern;

import org.springframework.util.StringUtils;

import com.tsquare.maven.plugins.appconfig.documentation.model.ConfigurationMetadata;
import com.tsquare.maven.plugins.appconfig.documentation.model.Group;
import com.tsquare.maven.plugins.appconfig.documentation.model.Property;

import io.github.swagger2markup.markup.builder.MarkupDocBuilder;
import io.github.swagger2markup.markup.builder.MarkupDocBuilders;
import io.github.swagger2markup.markup.builder.MarkupLanguage;
import io.github.swagger2markup.markup.builder.MarkupTableColumn;

public abstract class MarkupDocumentation implements Documentation {

   private static final Pattern TYPE_TRANSFORMER = Pattern.compile("(([^.<>,])+[.$])*([^<>,]*)");
   private static final int MAX_SECTION_LEVEL = 5;
   private static final String TOC_TEXT = "Configuration points";

   private final ConfigurationMetadata metadata;
   private final boolean external;
   private final boolean toc;

   public MarkupDocumentation(ConfigurationMetadata metadata, boolean external, boolean toc) {
      this.metadata = metadata;
      this.external = external;
      this.toc = toc;
   }

   public MarkupDocBuilder markup(MarkupLanguage language) {
      MarkupDocBuilder markup = MarkupDocBuilders.documentBuilder(language);

      GroupTree groupTree = new GroupTree(metadata.getGroups());

      markup.documentTitle("Configuration");

      if (toc) {
         buildToc(markup, groupTree);
      }

      buildGroup(markup, groupTree, 1);

      return markup;
   }

   private MarkupDocBuilder buildToc(MarkupDocBuilder markup, GroupTree groupTree) {
      markup.boldTextLine(TOC_TEXT + " :");
      markup.newLine();

      buildToc(markup, groupTree, 1);

      return markup;
   }

   protected MarkupDocBuilder buildToc(MarkupDocBuilder markup, GroupTree groupTree, int level) {
      groupTree.forEach((group, subgroup) -> {
         if (level > 0) {
            markup.text(String.join("", nCopies(level, "*")) + " ");
         }
         markup.crossReference(group.getName(), group.getName());
         markup.newLine(true);

         buildToc(markup, subgroup, level + 1);
      });

      return markup;
   }

   protected MarkupDocBuilder buildGroup(MarkupDocBuilder markup, GroupTree groupTree, int level) {
      groupTree.forEach((group, subGroups) -> {
         buildGroup(markup, group, level);
         buildGroup(markup, subGroups, level + 1);
      });

      return markup;
   }

   protected MarkupDocBuilder buildGroup(MarkupDocBuilder markup, Group group, int level) {

      markup.sectionTitleWithAnchorLevel(Math.min(level, MAX_SECTION_LEVEL),
                                         group.getName(), group.getName());

      markup.textLine("[horizontal]");
      if (!StringUtils.isEmpty(group.getDescription())) {
         markup.textLine("Description:: " + group.getDescription(), true);
      }
      markup.textLine("Prefix:: " + newMarkup(markup).literalText(group.getName()).toString());

      List<List<String>> properties = new ArrayList<>();

      Optional
            .ofNullable(metadata.getProperties())
            .ifPresent(p -> p
                  .stream()
                  .filter(property -> Objects.equals(property.getSourceType(), group.getType()))
                  .forEach(property -> properties.add(Arrays.asList(buildPropertyName(newMarkup(markup),
                                                                                      group,
                                                                                      property.getName()).toString(),
                                                                    buildDescription(newMarkup(markup),
                                                                                     property).toString()))));

      MarkupTableColumn[] columnSpecs = new MarkupTableColumn[] {
            new MarkupTableColumn("Key").withHeaderColumn(true).withMarkupSpecifiers(MarkupLanguage.ASCIIDOC,
                                                                                     "2a"),
            new MarkupTableColumn("Description").withMarkupSpecifiers(MarkupLanguage.ASCIIDOC, "5a") };

      if (properties.size() > 0) {
         markup.tableWithColumnSpecs(Arrays.asList(columnSpecs), properties);
      }

      if (!external) {
         markup.textLine("[horizontal]");
         markup.textLine("Type:: " + buildType(newMarkup(markup), group.getType(), false));
      }

      return markup;
   }

   protected MarkupDocBuilder buildPropertyName(MarkupDocBuilder markup, Group group, String name) {
      String prefix = group.getName() + ".";

      markup.literalText(name.startsWith(prefix) ? name.substring(prefix.length()) : name);

      return markup;
   }

   protected static MarkupDocBuilder buildType(MarkupDocBuilder markup, String type, boolean stripPackage) {
      if (!StringUtils.isEmpty(type)) {
         if (stripPackage) {
            markup.literalText(TYPE_TRANSFORMER.matcher(type).replaceAll("$3"));
         } else {
            markup.literalText(type);
         }
      }

      return markup;
   }

   protected MarkupDocBuilder buildDescription(MarkupDocBuilder markup, Property property) {
      if (property.getDescription() != null) {
         markup.textLine(property.getDescription(), true);
      }

      markup
            .boldText("Type")
            .text(" : ")
            .textLine(buildType(newMarkup(markup), property.getType(), true).toString(), true);
      if (property.getDefaultValue() != null) {
         MarkupDocBuilder defaultBuilder = markup.boldText("Default").text(" : ");
         String defaultText = property.getDefaultValue().toString();
         if (defaultText.isEmpty()) {
            defaultBuilder.newLine(true);
         } else {
            defaultBuilder.literalTextLine(defaultText, true);
         }
      }

      return markup;
   }

   protected MarkupDocBuilder newMarkup(MarkupDocBuilder markup) {
      return markup.copy(false);
   }

   protected class GroupTree extends TreeMap<Group, GroupTree> {

      public GroupTree() {
         this(new ArrayList<>());
      }

      /**
       * @implNote Only works because groups are sorted by name.
       */
      public GroupTree(List<Group> groups) {
         super(Comparator.comparing(Group::getName));

         Map<String, Group> groupsByType = new HashMap<>();
         groups.forEach(group -> groupsByType.put(group.getType(), group));

         groups.forEach(group -> {
            if (group.getType().equals(group.getSourceType())
                || groupsByType.get(group.getSourceType()) == null) {
               put(group, new GroupTree());
            } else {
               Group sourceGroup = groupsByType.get(group.getSourceType());

               computeIfAbsent(sourceGroup, __ -> new GroupTree()).put(group, new GroupTree());
            }
         });
      }

      @Override
      public String toString() {
         return entrySet()
               .stream()
               .map(group -> group.getKey() + " : " + group.getValue().toString())
               .collect(joining(",", "[", "]"));
      }
   }

}


