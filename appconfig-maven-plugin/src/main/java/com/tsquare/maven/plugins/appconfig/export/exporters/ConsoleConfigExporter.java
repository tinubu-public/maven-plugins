package com.tsquare.maven.plugins.appconfig.export.exporters;

import java.util.Properties;

import org.apache.maven.plugin.logging.Log;

import com.tsquare.maven.plugins.appconfig.export.properties.SortedProperties;

public class ConsoleConfigExporter implements ConfigExporter {

   private final Log log;

   public ConsoleConfigExporter(Log log) {
      this.log = log;
   }

   @Override
   public void export(String instance, String environment, Properties properties) {
      Properties sortedProperties = new SortedProperties();

      properties.forEach(sortedProperties::put);

      log.info(String.format("-- Display instance '%s' environment '%s'", instance, environment));

      sortedProperties.forEach((key, value) -> log.info(String.format("%s=%s", key, value)));
   }
}
