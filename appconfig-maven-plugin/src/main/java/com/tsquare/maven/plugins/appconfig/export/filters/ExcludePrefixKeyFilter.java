package com.tsquare.maven.plugins.appconfig.export.filters;

public class ExcludePrefixKeyFilter implements KeyFilter {
   private final String excludePrefix;

   public ExcludePrefixKeyFilter(String excludePrefix) {
      this.excludePrefix = excludePrefix;
   }

   @Override
   public boolean test(String key) {
      return !key.startsWith(excludePrefix);
   }
}
