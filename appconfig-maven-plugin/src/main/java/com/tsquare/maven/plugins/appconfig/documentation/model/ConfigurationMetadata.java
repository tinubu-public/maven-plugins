package com.tsquare.maven.plugins.appconfig.documentation.model;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ConfigurationMetadata {
   List<Group> groups;
   List<Property> properties;

   public List<Group> getGroups() {
      return groups;
   }

   public ConfigurationMetadata setGroups(List<Group> groups) {
      this.groups = groups;
      return this;
   }

   public List<Property> getProperties() {
      return properties;
   }

   public ConfigurationMetadata merge(ConfigurationMetadata metadata) {
      if (metadata.getGroups() != null) {
         List<Group> groups = getGroups() != null ? getGroups() : new ArrayList<>();

         groups.addAll(metadata.getGroups());
         setGroups(groups);
      }

      if (metadata.getProperties() != null) {
         List<Property> properties = getProperties() != null ? getProperties() : new ArrayList<>();

         properties.addAll(metadata.getProperties());
         setProperties(properties);
      }

      return this;
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", ConfigurationMetadata.class.getSimpleName() + "[", "]")
            .add("groups=" + groups)
            .add("properties=" + properties)
            .toString();
   }

   public ConfigurationMetadata setProperties(List<Property> properties) {
      this.properties = properties;
      return this;
   }

}
