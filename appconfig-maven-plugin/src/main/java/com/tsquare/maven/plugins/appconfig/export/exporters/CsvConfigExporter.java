package com.tsquare.maven.plugins.appconfig.export.exporters;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.maven.plugin.logging.Log;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.tsquare.maven.plugins.appconfig.export.properties.SortedProperties;

public class CsvConfigExporter implements ConfigExporter {

   private final Log log;
   private final Path exportPath;

   public CsvConfigExporter(Log log, Path exportPath) {
      this.log = log;
      this.exportPath = exportPath;
   }

   @Override
   @SuppressWarnings("ResultOfMethodCallIgnored")
   public void export(String instance, String environment, Properties properties) throws IOException {
      String identifier = String.format("%s-%s", instance, environment);
      File exportFile = exportPath.resolve(String.format("appconfig-%s.csv", identifier)).toFile();

      log.info(String.format("-- Export instance '%s' environment '%s' to '%s'",
                             instance,
                             environment,
                             exportFile));

      SortedProperties sortedProperties = new SortedProperties();
      properties.forEach(sortedProperties::put);

      exportFile.getParentFile().mkdirs();

      String csv = generateCsv(sortedProperties);

      try (PrintStream csvWriter = new PrintStream(exportFile)) {
         csvWriter.println(comment(identifier));
         csvWriter.println(comment(currentDateTime()));
         csvWriter.print(csv);
      }
   }

   private String generateCsv(SortedProperties sortedProperties) {
      CsvSchema schema = CsvSchema.builder().addColumn("key").addColumn("value").build().withColumnSeparator(';').withHeader();

      List<String[]> data = new ArrayList<>();
      sortedProperties.forEach((key, value) -> data.add(new String[] { (String) key, (String) value }));

      CsvMapper mapper = new CsvMapper();

      try {
         return mapper.writer(schema).writeValueAsString(data);
      } catch (JsonProcessingException e) {
         throw new IllegalStateException(e);
      }
   }

   private String comment(String comment) {
      return "# " + comment;
   }

}
