package com.tsquare.maven.plugins.appconfig.documentation.model;

import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Property {
   String sourceType;
   String type;
   String name;
   String description;
   Object defaultValue;

   public String getSourceType() {
      return sourceType;
   }

   public Property setSourceType(String sourceType) {
      this.sourceType = sourceType;
      return this;
   }

   public String getType() {
      return type;
   }

   public Property setType(String type) {
      this.type = type;
      return this;
   }

   public String getName() {
      return name;
   }

   public Property setName(String name) {
      this.name = name;
      return this;
   }

   public String getDescription() {
      return description;
   }

   public Property setDescription(String description) {
      this.description = description;
      return this;
   }

   public Object getDefaultValue() {
      return defaultValue;
   }

   public Property setDefaultValue(Object defaultValue) {
      this.defaultValue = defaultValue;
      return this;
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", Property.class.getSimpleName() + "[", "]")
            .add("sourceType='" + sourceType + "'")
            .add("type='" + type + "'")
            .add("name='" + name + "'")
            .add("description='" + description + "'")
            .add("defaultValue='" + defaultValue + "'")
            .toString();
   }
}
