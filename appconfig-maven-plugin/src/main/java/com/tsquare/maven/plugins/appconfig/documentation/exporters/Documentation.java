package com.tsquare.maven.plugins.appconfig.documentation.exporters;

import java.io.IOException;

public interface Documentation {
   void documentation() throws IOException;
}
